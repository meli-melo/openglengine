#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "shader_s.h"



void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{	//make sur the viewport matches the new window dimensions
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}


int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	//create GLFW window
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	//define opengl viewport size and a callback for when the window gets resized
	glViewport(0, 0, 800, 600);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	float verticesTriangle[] = {
		// positions         // colors
		 0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,   // bottom right
		-0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,   // bottom left
		 0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f    // top		
	};

	float verticesTriangles[] = {
		-1.0f, -0.5f, 0.0f, //bot left
		0.0f, -0.5f, 0.0f,	//bot right
		-0.5f, 0.5f, 0.0f, //mid
		0.0f, -0.5f, 0.0f,	//bot left
		1.0f, -0.5f, 0.0f,	//bot right
		0.5f, 0.5f, 0.0f	//mid
	};

	float verticesTriangle1[] = {
		-1.0f, -0.5f, 0.0f, //bot left
		0.0f, -0.5f, 0.0f,	//bot right
		-0.5f, 0.5f, 0.0f
	};

	float verticesTriangle2[] = {
		0.0f, -0.5f, 0.0f,	//bot left
		1.0f, -0.5f, 0.0f,	//bot right
		0.5f, 0.5f, 0.0f	//mid
	};

	float verticesRectangle[] = {
		0.5f, 0.5f, 0.0f,	//top right
		0.5f, -0.5f, 0.0f,	//bottom right
		-0.5f, -0.5f, 0.0f,	//bottom left
		-0.5f, 0.5f, 0.0f	//top left
	};

	unsigned int indicesRectangle[] = {
		0, 1, 3,	//first triangle
		1, 2, 3		//second triangle
	};

	//create new buffers
	unsigned int VBOTri1, VBOTri2, VAOTri1, VAOTri2, EBO;
	glGenVertexArrays(1, &VAOTri1);
	//glGenVertexArrays(1, &VAOTri2);
	glGenBuffers(1, &VBOTri1);
	//glGenBuffers(1, &VBOTri2);
	//glGenBuffers(1, &EBO);
	//bind vertex array object first, then bind and set vertex buffer(s), and then configure vertex attribute(s)
	glBindVertexArray(VAOTri1);

	glBindBuffer(GL_ARRAY_BUFFER, VBOTri1);
	//copy the data to the currently bound buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTriangle), verticesTriangle, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//bind vertex array object first, then bind and set vertex buffer(s), and then configure vertex attribute(s)
	//glBindVertexArray(VAOTri2);

	//glBindBuffer(GL_ARRAY_BUFFER, VBOTri2);
	//copy the data to the currently bound buffer
	//glBufferData(GL_ARRAY_BUFFER, sizeof(verticesTriangle2), verticesTriangle2, GL_STATIC_DRAW);

	//set the vertex attributes pointers
	//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	//glEnableVertexAttribArray(0);

	//copy indices into the element buffer object
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicesRectangle), indicesRectangle, GL_STATIC_DRAW);

	//note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//you can unbind the VAO afterward so other VAO calls won't accidentally modify this VAO, but this rarely happens.
	//Modifying other VAOs require a call to glBindVertexArray anyways so we generally don't unbind VAO's (nor VBOs) when it is not directly necessary.
	glBindVertexArray(0);

	//create color for our fragment shader uniform
	//float timeValue = glfwGetTime();
	//float greenValue = (sin(timeValue) / 2.0f) + 0.5f;
	//int vertexColorLocation = glGetUniformLocation(shaderProgramOrange, "ourColor");


	//uncomment this to draw in wireframe polygons.
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	Shader ourShader("shaders/vertexShader.vs", "shaders/customColor_fragmentShader.fs");
	

	//render loop
	while (!glfwWindowShouldClose(window))
	{
		//inputs
		processInput(window);


		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//glUseProgram(shaderProgramOrange);
		ourShader.use();
		//ourShader.setFloat("hOffset", 0.2);

		//send the custom color to the shader
		//glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0f, 1.0f);
		//update the custom color
		//timeValue = glfwGetTime();
		//greenValue = (sin(timeValue) / 2.0f) + 0.5f;

		//bind VAO of object to draw
		glBindVertexArray(VAOTri1);
		//draw object from elemnt buffer object bound to the VAO
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		//switch the shader to yellow to display the triangle in that color
		//glUseProgram(shaderProgramYellow);
		//bind VAO of object to draw
		//glBindVertexArray(VAOTri2);
		//draw object from elemnt buffer object bound to the VAO
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		//glDrawArrays(GL_TRIANGLES, 0, 6);
		//glBindVertexArray(0); //no need to unbind it every time

		//glfw: swap buffers and poll IO events (keys pressed/released, mouse moved, etc.)
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	//optional: de-allocate all resources once they have outlived their purpose:
	glDeleteVertexArrays(1, &VAOTri1);
	glDeleteVertexArrays(1, &VAOTri2);
	glDeleteBuffers(1, &VBOTri1);
	glDeleteBuffers(1, &VBOTri2);

	//glfw: terminate, clearing all previously allocated GLFW resources.
	glfwTerminate();
	return 0;
}